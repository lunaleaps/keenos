from pymongo import Connection
from bson import json_util
import json
import datetime
from bottle import route, static_file, run, request, redirect, abort, response
from logger import Logger
log = Logger()

###############################################################

@route('/')
def home():
	return static_file("myblog.html", root="./starter-kit")

@route('/populate')
def populate():
	connection = Connection()
	keenos = connection.keenos
	blogPostCollection = keenos['blog.post']

	posts = blogPostCollection.find()
	postList = []
	for post in posts:
		postList.append(post)
	return json.dumps(postList, default=json_util.default)

@route('/publish', method='POST')
def publish():
	postTitle = request.forms.get('title')
	postContent = request.forms.get('body')
	postTags = request.forms.get('tags')
	#postDate = datetime.date.today();

	log.warning("title :" + postTitle)
	log.warning("body :" + postContent)
	log.warning("tags :" + postTags)

	connection = Connection() # default to 'localhost' and port 27017
	keenos = connection.keenos
	blogPostCollection = keenos['blog.post']

	post = {"title": postTitle,
			"content": postContent,
			"tags": postTags}
			#"date": postDate 
	blogPostCollection.insert(post)

	# do some logic to verify the post...
	return json.dumps(post, default=json_util.default)

@route('/<filename:path>', method="GET")
def loadJavascript(filename):
	if (filename != None):
		log.warning("get"+filename)
		return static_file(filename, root='starter-kit/')
	abort(404, "File not found")

###############################################################

#@route('/home')
#def homepage():
	#show home page
'''
@route('/<state>')
def index(state):
	if(state == 'default'):
		return static_file("myblog.html", root="./starter-kit")
	if(state =='test'):
		return static_file("testAjax.html", root="./starter-kit")

def make_text(somejunk):
	return somejunk;

@route('/login', method="POST")
def login():
	log.warning("logging in")
	username = request.forms.get('username')
	password = request.forms.get('password')
	if username == 'luna' and password == 'pass':
		# return blog posts in json format
		return '{"blogPostName": "postName", "dateCreated": "Juanayr 24"}'
	return None

@route('/<filename:path>', method="GET")
def loadJavascript(filename):
	log.warning("js")
	if (filename != None):
		log.warning("get"+filename)
		return static_file(filename, root='starter-kit/')
	abort(404, "File not found")

@route('/ajaxExample', method='POST')
def ajax():
	log.warning("we got into ajaxExample")
	inputStuff = request.forms.word
	if( inputStuff != ""):
		log.warning("request.forms.word == ")
		return inputStuff
	return "empty input"'''

run(host='localhost', port=8084, debug=True)
