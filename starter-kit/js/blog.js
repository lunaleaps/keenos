/* Ember Code */

var Keenos = Em.Application.create();

Keenos.BlogPost = Em.Object.extend({
	title: null,
	content: null,
	date: new Date(0),
	tags: null
});

Keenos.blogPostController = Em.ArrayController.create({
	content: [],
	addPost: function(blogPost) {
		this.pushObject(blogPost);
	}
});

Keenos.BlogListView = Em.View.extend({
	classNames: ['post']
});

$('input[name="addPost"]').click(function(event) {
	$('div#writeContainer').attr("hidden", false);
});

$('input[name="publishPost"]').click(function(event){
	$.post('/publish', createPost(), function(data){
		populate();
		$('div#writeContainer').attr("hidden", true);
	});
});

$('input[name="cancelPost"]').click(function(event) {
	$('div#writeContainer').attr("hidden", true);
});

function createPost() {
	var post = {title: $('input[name="postTitle"]').val(),
				body: $('textarea#postBody').val(),
				tags: $('input[name="postTags"]').val() };
	return post;
}

$(document).ready(function(){
	populate();
});

function populate(){
	$.get('/populate', function(data){
		var posts = $.parseJSON(data);
		Keenos.blogPostController.set('content', posts);
	});
};

/*
$('input[name="login"]').click(function(event) {	
	$('div#loginBox').show();
	$('input[name="login"]').hide();
});

$('input[name="cancelLogin"]').click(function(event){
	$('div#loginBox').hide();
	$('input[name="login"]').show();
});

$('input[name="realLogin"]').click(function(event) {
	$.post('/login', { username: $('input[name="username"]').val(), 
		  password: $('input[name="password"]').val() 
	}, function(data){
		var post = $.parseJSON(data);
		if post != 
		alert("name : " + cat.name + " age: " + cat.age + " breed: " + cat.breed)
	});
}); */
