import logging 

class Logger:
    def __init__(self):
        self.logger = logging.getLogger('log')
        self.hdlr = logging.FileHandler('log.log')
        self.formatter = logging.Formatter('%(asctime)s %(levelname)s %(message)s')
        self.hdlr.setFormatter(self.formatter)
        self.logger.addHandler(self.hdlr)
        self.logger.setLevel(logging.WARNING)

    def error(self, message):
        self.logger.error(message)

    def warning(self, message):
        self.logger.warning(message)